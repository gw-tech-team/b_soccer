import collections
import cv2
from decimal import Decimal, ROUND_HALF_UP
import numpy as np
import os

input_text_file = "yolo_texts/yolo_format_all_resize_0813.txt"
output_text_file = "yolo_texts/yolo_format_aug_B_all_0813.txt"
output_image_dir = ""
class_list = ["0","1","2","3","4","5","6","7","8","9"]
elminate_list = ["10","11","12","13","14"]
input_shape = (3840,2160) #(w,h)
split_A = (9,5)
split_B = (3,2)
darknet_size = 32
"/media/shared_data/all"
aug_ratio = 3
class_switch = False
output_dir = "obj_0813/"

def count_class():
    """
    class_result returns total number of each in dict form
    e.g
    class_result = {"1":123,"2:"}
    """

    class_counter = []
    f = open(input_text_file,"r")
    for line in f.readlines():
        image_info = line.split(" ")
        for k in range(1,len(image_info)-2):
            bbox = image_info[k].split(",")
            class_id = bbox[4]
            if class_id in class_list:
                class_counter.append(class_id)
    class_result = collections.Counter(class_counter)
    return class_result

def get_crop_num(class_result,flip=False):
    """
    This function returns crop number based on data distribution
    so that number of each class balnaces out
    """

    max_class_num = max(class_result.values())
    crop_num_dic = {}
    for key in class_result.keys():
        if flip == True:
            crop_num = max_class_num/int(class_result[key])/2
        else:
            crop_num = max_class_num*aug_ratio/int(class_result[key])
        print(crop_num)
        crop_num = Decimal(str(crop_num)).quantize(Decimal('0'), rounding=ROUND_HALF_UP)
        print("class_id:{},crop_nu,:{}".format(key,crop_num))
        crop_num_dic[key] = crop_num
    return crop_num_dic

def flip(image,bbox):
    """
    Just a flip function
    """
    # flip bbox coordinates
    x1 = input_shape[0] - bbox[2]
    y1 = bbox[1]
    x2 = input_shape[0] - bbox[0]
    y2 = bbox[3]
    #flip image
    image = image[:, ::-1, :]
    return image,(x1,y1,x2,y2)



def crop(image,bbox,crop_size,flip):
    """
    It crops an image and returns image and coordinates
    around bbox
    """
    h,w,_ = image.shape
    # print(image.shape)
    # bbox coordinates
    x1 = bbox[0]
    y1 = bbox[1]
    x2 = bbox[2]
    y2 = bbox[3]
    #determine top,left,bottom,right
    while True:
        top_min = y2-crop_size[1]
        top_max = y1
        if top_min <= 0:
            top_min = 0
        if top_max == 0:
            top = 0
        else:
            top = np.random.randint(top_min,top_max)

        left_min = x2-crop_size[0]
        left_max = x1
        if left_min <= 0:
            left_min = 0
        if left_max == 0:
            left = 0
        else:
            left = np.random.randint(left_min,left_max)

        bottom = top + crop_size[1]
        right = left + crop_size[0]
        # print("top:{} left:{} bottom:{} right{} y2:{} w:{} h:{} x1:{} y1:{} x2:{} y2:{}".format(top,left,bottom,right,y2,w,h,x1,y1,x2,y2))
        if flip == True:
            if 0 <= top and 0 <= right and bottom <= h and left <= w:
                break
        else:
            if 0 <= top and 0 <= left and bottom <= h and right <= w:
                break
    #Crop
    img = image[top:bottom, left:right, :]

    #Create new coodinates to suit new image
    new_top = y1 - top
    new_left = x1 - left
    new_bottom = y2 - top
    new_right = x2 - left
    if new_top > 416 or new_left > 416:
        print("what????")
        print("top,left,right,bottom",top,left,right,bottom)
        print("x1,y1,x2,y2",x1,y1,x2,y2)
        print("new_top,new_left,new_bottom,new_right",new_top,new_left,new_bottom,new_right)


    return img,(new_left,new_top,new_right,new_bottom),(left,top,right,bottom)

def scan_coexistances(line,bbox,id_info,path_base):
    """
    It returns a class and the coordinates coexisting in the specified bbox in list form
    id_info contains the info that can identify original bbox so that
    coex values does not contain the augmented bbox
    This time id_info is (x1,x2) Suppose it's not perfect but sufficient
    """
    coex = []
    bbox_info = line.split(" ")
    if "00000390" in path_base:
        with open("log.txt",mode="a") as log:
            log.write(path_base + "\n")
            log.write(str(bbox_info) + "\n")
    for k in range(1,len(bbox_info)):
        bbox_init = bbox_info[k].split(",")
        if bbox_init[0] != "\n":
            """Get all classes in the original image"""
            x1_init = int(bbox_init[0])
            y1_init = int(bbox_init[1])
            x2_init = int(bbox_init[2])
            y2_init = int(bbox_init[3])
            class_id = bbox_init[4]
            if "\n" in class_id:
                class_id = class_id.replace("\n","")
            """Get frame data"""
            x1_crop = int(bbox[0])
            y1_crop = int(bbox[1])
            x2_crop = int(bbox[2])
            y2_crop = int(bbox[3])
            if class_switch == True:
                if class_id in class_list:
                    """elminate the same bbox"""
                    if x1_init == id_info[0] and x2_init == id_info[2] and y1_init == id_info[1] and y2_init == id_info[3]:
                        pass
                    else:
                        """For debug"""
                        if "00000390" in path_base:
                            with open("log.txt",mode="a") as log:
                                crop_bbo = "{},{},{},{}".format(x1_crop,y1_crop,x2_crop,y2_crop)
                                log.write("(x1_crop,y1_crop,x2_crop,y2_crop):" + crop_bbo + "\n")
                                init_bbo = "{},{},{},{}".format(x1_init,y1_init,x2_init,y2_init)
                                log.write("(x1_init,y1_init,x2_init,y2_init):" + init_bbo + "\n")
                        """See if another class is in the same frame"""
                        if x1_crop <= x1_init and y1_crop <= y1_init and x2_init <= x2_crop and y2_init <= y2_crop:
                            coex.append((x1_init,y1_init,x2_init,y2_init,class_id))
            else:
                if class_id != "14":
                    """elminate the same bbox"""
                    if x1_init == id_info[0] and x2_init == id_info[2] and y1_init == id_info[1] and y2_init == id_info[3]:
                        pass
                    else:
                        """For debug"""
                        if "00000390" in path_base:
                            with open("log.txt",mode="a") as log:
                                crop_bbo = "{},{},{},{}".format(x1_crop,y1_crop,x2_crop,y2_crop)
                                log.write("(x1_crop,y1_crop,x2_crop,y2_crop):" + crop_bbo + "\n")
                                init_bbo = "{},{},{},{}".format(x1_init,y1_init,x2_init,y2_init)
                                log.write("(x1_init,y1_init,x2_init,y2_init):" + init_bbo + "\n")
                        """See if another class is in the same frame"""
                        if x1_crop <= x1_init and y1_crop <= y1_init and x2_init <= x2_crop and y2_init <= y2_crop:
                            coex.append((x1_init,y1_init,x2_init,y2_init,class_id))
    return coex


def model_option(model):
    """
    It returns width and height that are suitable for this project and darknet
    """

    if model == "A":
        w = input_shape[0]/split_A[0]/darknet_size
        w = Decimal(str(w)).quantize(Decimal('0'), rounding=ROUND_HALF_UP)
        w = int(w*darknet_size)
        h = input_shape[1]/split_A[1]/darknet_size
        h = Decimal(str(h)).quantize(Decimal('0'), rounding=ROUND_HALF_UP)
        h = int(h*darknet_size)
    elif model == "B":
        w = input_shape[0]/4
        h = input_shape[1]/4
        w = w/split_B[0]/darknet_size
        w = Decimal(str(w)).quantize(Decimal('0'), rounding=ROUND_HALF_UP)
        w = int(w*darknet_size)
        h = h/split_B[1]/darknet_size
        h = Decimal(str(h)).quantize(Decimal('0'), rounding=ROUND_HALF_UP)
        h = int(h*darknet_size)
    return (w,h)

def bbox_format(x1,y1,x2,y2,class_id):
    bbox_format = str(x1) + "," + str(y1) + "," + str(x2) + "," + str(y2) + "," + class_id
    return bbox_format


def main():
    w,h = (416,416) #model_option("A")
    crop_size = (w,h)
    print(crop_size)
    class_result = count_class()
    crop_num_dic = get_crop_num(class_result)
    print(crop_num_dic)
    print(crop_num_dic["1"])
    print(class_result)
    print(max(class_result.values()))
    max_class_num = max(class_result.values())
    flip_mode = False


    print("crop_num_dic",crop_num_dic)

    os.makedirs(output_dir, exist_ok=True)

    f = open(input_text_file, mode='r')
    lines = f.readlines()

    with open(output_text_file,"w") as g:
        g.write("")

    with open("log.txt","w") as log:
        log.write("")

    for i, line in enumerate(lines):
        # print(line)
        print("process{}/{}".format(i,len(lines)))
        bbox_info = line.split(" ")
        # print(bbox_info)
        bbox_info.remove("\n")
        path = bbox_info[0]
        # path = path.replace("data/","/media/shared_data/")
        img = cv2.imread(path)
        img_init = img.copy()

        path_base = os.path.basename(path).replace(".jpg","")

        with open(output_text_file,"a") as g:
            for k in range(1,len(bbox_info)):
                bbox = bbox_info[k].split(",")
                x1_init = int(bbox[0])
                y1_init = int(bbox[1])
                x2_init = int(bbox[2])
                y2_init = int(bbox[3])
                if x1_init > 10000 or y1_init > 10000 or x2_init > 10000 or y2_init > 10000:
                    break
                class_id = bbox[4]
                if "\n" in class_id:
                    class_id = class_id.replace("\n","")
                id_info = (x1_init,y1_init,x2_init,y2_init)
                if class_id in class_list:
                    crop_num = crop_num_dic[class_id]

                    """Crop original image"""
                    l = 0
                    for l in range(int(crop_num)):
                        img_crop,(x1_crop,y1_crop,x2_crop,y2_crop),(left,top,right,bottom) = crop(img_init,(x1_init,y1_init,x2_init,y2_init),crop_size,flip=False)
                        save_path = output_dir + path_base + "_o_" + class_id + "_" + str(k) + "_" + str(l) + "_" + str(i) + ".png"
                        cv2.imwrite(save_path,img_crop)
                        bbox_form = bbox_format(x1_crop,y1_crop,x2_crop,y2_crop,class_id)
                        g.write(save_path + " " + bbox_form)

                        coex = scan_coexistances(line,(left,top,right,bottom),id_info,path_base)

                        if len(coex) != 0:
                           for coex_bbox in coex:
                               x1_coex = int(coex_bbox[0]-left)
                               y1_coex = int(coex_bbox[1]-top)
                               x2_coex = int(coex_bbox[2]-left)
                               y2_coex = int(coex_bbox[3]-top)
                               if x2_coex > 416 or y2_coex >416:
                                   print("something's wrong: x1_coex:{}  y1_coex:{} x2_coex:{} y2_coex:{}".format(x1_coex,y1_coex,x2_coex,y2_coex))
                                   print("save_path:",save_path)
                               class_id_coex = coex_bbox[4]
                               bbox_form_coex = bbox_format(x1_coex,y1_coex,x2_coex,y2_coex,class_id_coex)
                               g.write(" " + bbox_form_coex)
                        g.write(" " + "\n")
                        if "00000390" in path_base:
                            with open("log.txt","a") as log:
                                log.write("save_path:" + save_path + "\n")
                                log.write("coex:" + str(coex) + "\n")
                                crop_co = "{},{},{},{}".format(left,top,right,bottom)
                                crop_bbo = "{},{},{},{}".format(x1_crop,y1_crop,x2_crop,y2_crop)
                                log.write("left,top,right,bottom:" + crop_co + "\n")
                                log.write("(x1_crop,y1_crop,x2_crop,y2_crop):" + crop_bbo + "\n")

                    """Flip original image and crop it"""
                    if flip_mode == True:
                        img_flip,(x1_flip,y1_flip,x2_flip,y2_flip) = flip(img_init,(x1_init,y1_init,x2_init,y2_init))
                        j = 0
                        for j in range(int(crop_num)):
                            img_crop,(x1_crop,y1_crop,x2_crop,y2_crop) = crop(img_flip,(x1_flip,y1_flip,x2_flip,y2_flip),crop_size,flip=True)
                            coex = scan_coexistances(line,(x1_crop,y1_crop,x2_crop,y2_crop),id_info)
                            save_path = output_dir + path_base + "_f_"  + class_id + "_" + str(k) + "_" + str(l) + ".png"
                            cv2.imwrite(save_path,img_crop)
                            bbox_form = bbox_format(x1_crop,y1_crop,x2_crop,y2_crop,class_id)
                            g.write(save_path + " " + bbox_form)
                            if len(coex) != 0:
                                for coex_bbox in coex:
                                    x1_coex = int(coex_bbox[0])
                                    y1_coex = int(coex_bbox[1])
                                    x2_coex = int(coex_bbox[2])
                                    y2_coex = int(coex_bbox[3])
                                    class_id = coex_bbox[4]
                                    bbox_form = bbox_format(x1_coex,y1_coex,x2_coex,y2_coex,class_id)
                                    g.write(" " + bbox_form)
                            g.write("\n")

if __name__ == "__main__":
    main()
    print("done")
