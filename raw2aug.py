"""
This is the script to convert yolo format into gw augmentation format
"""

import os
import cv2


def id_converter(class_id):
    if
    return class_id

def load_annotation_csv(annotation_file_path):
    import codecs
    import csv
    '''
        Load annotation csv file.
        @param annotation_file_path: Annotation file path.
        @param class_list: Crop class list.
        @return Return annotation data list.
    '''
    if not os.path.exists(annotation_file_path):
        print('annotation csv file not found. target name:',os.path.basename(annotation_file_path))
        return None
    with codecs.open(annotation_file_path, encoding='utf-8') as f:
        annotation_list = []
        reader = csv.reader(f)
        for record in reader:
            if len(record) == 0:
                continue
            annotation_list.append(record)
    return annotation_list

def main():
    f = open("./test.txt")
    save_dir = "./aug_before/"

    for i, line in enumerate(f.readlines()):
        print("process",i)
        bbox_info = line.split(" ")
        path = bbox_info[0]

        """Copy data in your specified dirrectory"""
        img = cv2.imread(path)
        """Create csv file"""
        path = os.path.basename(path)
        img_h, img_w, _ = img.shape
        cv2.imwrite(save_dir + path,img)

        with open(save_dir + path.replace(".jpg",".csv"), mode='w') as j:
            j.writelines("")

        for k in range(1,len(bbox_info)-2):
            bbox = bbox_info[k].split(",")
            x1 = int(bbox[0])
            y1 = int(bbox[1])
            x2 = int(bbox[2])
            y2 = int(bbox[3])
            class_id = bbox[4]
            class_id = id_converter(class_id)
            h = y2 - y1
            w = x2 - x1
            with open(save_dir +path.replace(".jpg",".csv"), mode='a') as j:
                format = "''," + str(x1) + "," + str(y1) + "," + str(w) + "," + str(h) + "," + str(class_id) + ",''," + "False" + "\n"
                j.write(format)

def sub():
    record = load_annotation_csv("test.csv")
    print(record)

if __name__ == "__main__":
    main()
    # sub()
