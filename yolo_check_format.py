import cv2
import os
import random
class_list = ["No.0","No.1","No.2","No.3","No.4","No.5","No.6","No.7","No.8","No.9",
                "Referee","Head","Person","Ball","None"]

mode = "a"

if mode == "a":
    file = "yolo_texts/yolo_format_all_resize_0813.txt"
    output_dir = "./check_13_3/"
    os.makedirs(output_dir, exist_ok=True)
    single = False
elif mode == "b":
    file = "darknet_result.txt"
    output_dir = "./result_resize/"
    os.makedirs(output_dir, exist_ok=True)
    single = False

f = open(file)

for q,line in enumerate(f.readlines()):
    # print("process:{}/{}".format(q,len(f.readlines())))
    line = line.split(" ")
    i = 0
    for i, bbox_info in enumerate(line):
        if single == True:

            if i == 0:
                path = bbox_info
            if "/00033390_o_6_33_4_932.png" in path:
                print("!!!!!!!!!!   ")
                if i == 0:
                    img = cv2.imread(path)
                    img_h,img_w,_ = img.shape
                else:
                    if 0 < len(bbox_info):
                        if bbox_info[0] != "\n":
                            bbox = bbox_info.split(",")
                            print(bbox)
                            x1 = int(bbox[0])
                            y1 = int(bbox[1])
                            x2 = int(bbox[2])
                            y2 = int(bbox[3])
                            if x1 > img_w or x2 > img_w or y1 > img_h or y2 > img_h:
                                print("wrong")
                                print("x1,x2,y1,y2:",x1,x2,y1,y2)
                            if x1 < 10000 and y1 < 10000 and x2 < 10000 and y2 < 10000:
                                class_id = str(bbox[4])
                                print("x1,y1,x2,y2",x1,y1,x2,y2)
                                img = cv2.rectangle(img, (x1,y1),(x2, y2), (255,0,0),2)
                                img = cv2.putText(img, text=class_list[int(class_id)], org=(x1, y1), fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                                                fontScale=0.5, color=(255, 0, 0), thickness=1)
                                print("len(line)-1",len(line)-1)
                        else:
                            print(output_dir + "_" + str(i) + os.path.basename(path))
                            print("Saved!")
                            cv2.imwrite(output_dir + "_" + str(i) + os.path.basename(path),img)

        else:

            if i == 0:
                path = bbox_info
                img = cv2.imread(path)
                img_h,img_w,_ = img.shape
            else:
                if 0 < len(bbox_info):
                    if bbox_info[0] != "\n":
                        bbox = bbox_info.split(",")
                        x1 = int(bbox[0])
                        y1 = int(bbox[1])
                        x2 = int(bbox[2])
                        y2 = int(bbox[3])
                        class_id = str(bbox[4])
                        if x1 > img_w or x2 > img_w or y1 > img_h or y2 > img_h:
                            print(path)
                            print("wrong")
                            print("x1,x2,y1,y2:",x1,x2,y1,y2)
                            print("img_h,img_w",img_h,img_w)
                        if x1 < 10000 and y1 < 10000 and x2 < 10000 and y2 < 10000:
                            img = cv2.rectangle(img, (x1,y1),(x2, y2), (255,0,0),2)
                            img = cv2.putText(img, text=class_list[int(class_id)], org=(x1, y1), fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                                            fontScale=0.5, color=(255, 0, 0), thickness=1)
                    else:
                        cv2.imwrite(output_dir + "_" + str(i) + os.path.basename(path),img)
