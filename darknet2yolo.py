import os
import glob

def darknet2yolo(x,y,w,h,input_shape):
    width = w*input_shape[0]
    height = h*input_shape[1]
    x_center = x*input_shape[0]
    y_center =  y*input_shape[1]
    x1 =  int(x_center - width/2)
    y1 = int(y_center - height/2)
    x2 = int(x1 + width)
    y2 = int(y1 + height)
    return x1,y1,x2,y2

def bbox_format(x1,y1,x2,y2,class_id):
    bbox_string = str(x1) + "," + str(y1) + "," + str(x2) + "," + str(y2) + "," + class_id
    return bbox_string

def main():
    project_name = "0727"
    val_text = project_name + "/val.txt"
    output_path = "darknet_result.txt"
    input_shape = (320,256)

    f = open(val_text,mode="r")
    lines = f.readlines()
    with open(output_path,mode="w") as g:
        for img_path in lines:
            """Write a path to image"""
            img_path = img_path.replace("data",project_name)
            if "\n" in img_path:
                img_path = img_path.replace("\n","")
            g.write(img_path + " ")

            """Get bbox info from text"""
            text_path = img_path.replace(".png",".txt").replace("obj_0727","results")
            h = open(text_path,mode="r")
            bboxes =h.readlines()
            if 0 < len(bboxes):
                for i, bbox in enumerate(bboxes):
                    bbox = bbox.split(" ")
                    class_id = bbox[0]
                    x = float(bbox[1])
                    y = float(bbox[2])
                    w = float(bbox[3])
                    h = float(bbox[4].replace("\n",""))
                    x1,y1,x2,y2 = darknet2yolo(x,y,w,h,input_shape)
                    bbox_string = bbox_format(x1,y1,x2,y2,class_id)

                    """Write bbox info on new text_file"""
                    if i == len(bboxes)-1:
                        g.write(bbox_string+" "+"\n")
                    else:
                        g.write(bbox_string+" ")
            else:
                g.write(" " + "\n")

if __name__ == "__main__":
    main()
