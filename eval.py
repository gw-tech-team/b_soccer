import os
import cv2
import matplotlib.pyplot as plt
from tqdm import tqdm
from operator import itemgetter

class_list = ["0","1","2","3","4","5","6","7","8","9",
                "Referee","Head","Person","Ball","None"]
color_label = [(255,0,0),(0,255,0),(0,0,255),(255,255,0),(0,100,100),(0,255,255),(255,0,255),(0,0,0),(100,100,100),(100,0,100),
                (100,100,0),(0,100,100),(0,0,100),(0,100,0),(0,50,0)]
plt_lable =  [(1,0,0),(0,1,0),(0,0,1),(1,1,0),(0,0.5,0.5),(0,1,1),(1,0,1),(0,0,0),(0.5,0.5,0.5),(0.5,0,0.5),
                (1,1,0),(0,1,1),(0,0,1),(0,1,0),(0,1,0)]

def draw_result(save_path,img_path,og_img,gt_img,pred_img,gt_bboxes,pred_bboxes):
    og_img = cv2.cvtColor(og_img, cv2.COLOR_BGR2RGB)
    plt.figure(figsize=(12,10))
    plt.subplot(221)
    plt.title('Original')
    plt.imshow(og_img)
    plt.subplot(222)
    plt.title('Ground Truth')
    plt.imshow(gt_img)
    plt.subplot(223)
    plt.title('Prediction')
    plt.imshow(pred_img)
    plt.subplot(223)
    x, y, _ = og_img.shape
    slide = 0
    gt_bboxes = sorted(gt_bboxes, key=itemgetter(4))
    pred_bboxes = sorted(pred_bboxes, key=itemgetter(4))
    for i, gt_bbox in enumerate(gt_bboxes):
        class_id = gt_bbox[4]
        plt.text(x+x*0.2,y*(i+1)/20,"Ground Truth:" + str(gt_bbox),fontsize=x*0.02,color=plt_lable[int(class_id)])
        slide += 1
    # plt.text(x+x*0.4,y*(slide+1)/20,"*"*10,fontsize=x*0.05)
    for j, pred_bbox in enumerate(pred_bboxes):
        class_id = pred_bbox[4]
        plt.text(x+x*1.1,y*(j+1)/20,"Prediction:" + str(pred_bbox),fontsize=x*0.02,color=plt_lable[int(class_id)])
    name = save_path + img_path
    plt.savefig(name)
    plt.close()

def darknet2yolo(x,y,w,h,input_shape):
    width = w*input_shape[0]
    height = h*input_shape[1]
    x_center = x*input_shape[0]
    y_center =  y*input_shape[1]
    x1 =  int(x_center - width/2)
    y1 = int(y_center - height/2)
    x2 = int(x1 + width)
    y2 = int(y1 + height)
    return x1,y1,x2,y2

def get_gt(input_shape,og_img,img_path,gt_text_dir):
    """
    returns
    gt_img: image with bboxes of ground truth
    gt_bboxes_dic={"path_1":[x1,y1,x2,y2,class_id],[x1,y2,...]}
    """
    text_path = gt_text_dir + "/" + img_path.replace(".png",".txt")
    f = open(text_path,mode="r")
    lines = f.readlines()
    gt_bboxes = []
    base_img_gt = og_img.copy()
    base_img_gt = cv2.cvtColor(base_img_gt, cv2.COLOR_BGR2RGB)
    for i, line in enumerate(lines):
        bbox = line.split(" ")
        class_id = str(bbox[0])
        x = float(bbox[1])
        y = float(bbox[2])
        w = float(bbox[3])
        h = float(bbox[4].strip("\n"))
        x1,y1,x2,y2 = darknet2yolo(x,y,w,h,input_shape)
        gt_bboxes.append((x1,y1,x2,y2,class_id))
        if i == 0:
            gt_img = cv2.rectangle(base_img_gt, (x1,y1),(x2, y2), color_label[int(class_id)],2)
        else:
            gt_img = cv2.rectangle(gt_img, (int(x1),int(y1)),(int(x2), int(y2)), color_label[int(class_id)],2)
        gt_img = cv2.putText(gt_img, text=class_list[int(class_id)], org=(x1, y1), fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                        fontScale=0.5, color=color_label[int(class_id)], thickness=2)
    return gt_img,gt_bboxes

def get_pred(input_shape,og_img,img_path,pred_text_dir):
    """
    returns
    gt_img: image with bboxes of ground truth
    gt_bboxes_dic={"path_1":[x1,y1,x2,y2,class_id],[x1,y2,...]}
    """
    text_path = pred_text_dir + "/" + img_path.replace(".png",".txt")
    f = open(text_path,mode="r")
    lines = f.readlines()
    pred_bboxes = []
    base_img_pred = og_img.copy()
    base_img_pred = cv2.cvtColor(base_img_pred, cv2.COLOR_BGR2RGB)
    if 0 < len(lines):
        for i, line in enumerate(lines):
            bbox = line.split(" ")
            class_id = str(bbox[0])
            x = float(bbox[1])
            y = float(bbox[2])
            w = float(bbox[3])
            h = float(bbox[4].strip("\n"))
            x1,y1,x2,y2 = darknet2yolo(x,y,w,h,input_shape)
            pred_bboxes.append((x1,y1,x2,y2,class_id))
            if i == 0:
                pred_img = cv2.rectangle(base_img_pred, (x1,y1),(x2, y2), color_label[int(class_id)],2)
            else:
                pred_img = cv2.rectangle(pred_img, (x1,y1),(x2, y2), color_label[int(class_id)],2)
            pred_img = cv2.putText(pred_img, text=class_list[int(class_id)], org=(x1, y1), fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                            fontScale=0.5, color=color_label[int(class_id)], thickness=2)
    else:
        pred_img = base_img_pred
    return pred_img,pred_bboxes

def main():

    # test_img_list = "result_store/0810/val.txt"
    # gt_text_dir = "result_store/0810/obj_0805"
    # pred_text_dir = gt_text_dir #"0810/results"
    # img_dir = "result_store/0810/obj_0805"
    test_img_list = "C:/darknet/data/val.txt"
    gt_text_dir = "val_0805"
    pred_text_dir = "obj_0805" #"0810/results"
    img_dir = "obj_0805"
    save_path = "eval_0805/"
    os.makedirs(save_path, exist_ok=True)

    """Get refilned paths for test imgs"""
    f = open(test_img_list)
    img_list_before = f.readlines()
    img_list_after = []
    for img_path in img_list_before:
        img_path = os.path.basename(img_path).strip("\n")
        img_list_after.append(img_path)

    for img_path in tqdm(img_list_after):
        # print("img_path",img_path)
        og_img = cv2.imread(img_dir + "/" + img_path)
        h,w,_ = og_img.shape
        input_shape = (w,h)
        gt_img, gt_bboxes = get_gt(input_shape,og_img,img_path,gt_text_dir)
        pred_img, pred_bboxes = get_pred(input_shape,og_img,img_path,pred_text_dir)
        draw_result(save_path,img_path,og_img,gt_img,pred_img,gt_bboxes,pred_bboxes)

if __name__ == "__main__":
    main()
