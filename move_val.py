import shutil
import os
from tqdm import tqdm

# f = open("format_files/obj_0813/val.txt")
# lines = f.readlines()
#
# new_dir = "dummy/"
# os.makedirs(new_dir,exist_ok=True)
#
# for line in tqdm(lines):
#     text_path = line.strip("data/").replace(".png",".txt").strip("\n")
#     shutil.copy(text_path, new_dir)

def main():
    f = open("format_files/obj_0813/train.txt")
    # new_dir = "C:\darknet\data\obj_0805"
    new_dir = "D:/H_F/master/train_0813_image"
    os.makedirs(new_dir,exist_ok=True)
    lines = f.readlines()
    print(lines[0])
    for line in tqdm(lines):
        # text_path = line.strip("data/").replace(".png",".txt").strip("\n")
        text_path = line.strip("\n").strip("data/")
        shutil.copy(text_path, new_dir)

if __name__ == "__main__":
    main()
