import os
import cv2
import collections

def main():
    input_file = "yolo_texts/yolo_format_aug_B_all_0813.txt"
    output_dir = "obj_0813/"
    size = (416, 416)
    count_list = []
    train = 0.9
    train_text_path = "format_files/" + output_dir
    os.makedirs(train_text_path, exist_ok=True)

    f = open(input_file)
    with open(train_text_path + "train.txt", mode='w') as g:
        g.writelines("")
    with open(train_text_path + "val.txt", mode='w') as h:
        h.writelines("")
    lines = f.readlines()
    whole_length = len(lines)
    train_num = whole_length*train
    for i, line in enumerate(lines):
        print("process{}/{}".format(i,whole_length))
        bbox_info = line.split(" ")
        path = bbox_info[0]
        img = cv2.imread(path)
        img_h, img_w, _ = img.shape
        path = os.path.basename(path)
        with open(output_dir+path.replace(".png",".txt"), mode='w') as j:
            j.writelines("")
        if i < train_num:
            with open(train_text_path + "train.txt", mode='a') as g:
                g.write("data/" + output_dir  + path + "\n")
        else:
            with open(train_text_path + "val.txt", mode='a') as h:
                h.write("data/" + output_dir + path + "\n")
        for k in range(1,len(bbox_info)):
            bbox = bbox_info[k].split(",")
            if bbox[0] != "\n":
                x1 = int(bbox[0])
                y1 = int(bbox[1])
                x2 = int(bbox[2])
                y2 = int(bbox[3])
                class_id = bbox[4]
                count_list.append(class_id)
                x_center = (x1 + (x2 - x1) / 2) / size[0]
                y_center = (y1 + (y2 - y1) / 2) / size[1]
                width = (x2 - x1) / size[0]
                height = (y2 - y1) / size[1]
                if x_center > 1 or y_center > 1:
                    print("something is wrong")
                    print("path:",path)
                if x1 > 416 or y1 > 416:
                    print("Big")
                    print("x1:{} y1:{}".format(x1,y1))

                # print(x1,x2,y1,y2,img_w,img_h)
                # print(str(class_id) + " " + str(x_center) + " " + str(y_center) + " " + str(width) + " " + str(height) + "\n")
                with open(output_dir+path.replace(".png",".txt"), mode='a') as j:
                    j.write(str(class_id) + " " + str(x_center) + " " + str(y_center) + " " + str(width) + " " + str(height) + "\n")

    c = collections.Counter(count_list)
    print("counter of class_id:",c)

if __name__ == "__main__":
    main()
