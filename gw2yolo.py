# -*- coding: utf-8 -*-


'''
GW format:
<(矩形種別),左上X座標,左上Y座標,幅,高さ,タグ,(確信度),背番号,Occlusion>
→　()の項目は無視してください
→　タグが No-A,No-B,No-C ...と“No-“と付いているものが背番号矩形です
→　Occlusion:
　　斜めに写っていたり、遮蔽物がある等、
　　番号自体は判別が難しいものをTrueにしています。
　　（前後の画像から判斷して番号を入力しています）
"AREA","475.0","1225.0","58.0","136.0","Person","None","","False","False"
"RECT","483.0","1225.0","23.0","24.0","Head","None","False"
"AREA","1030.0","593.0","28.0","59.0","Person","None","","False(occlusion)","False(referee)"
"RECT"', '"3418.0"', '"1806.0"', '"18.0"', '"25.0"', '"No-A"', '"None"', '"6"', '"True"

None  無視
空　無視
後ろから２個め　オクルージョン False＝オクルージョンなし
一番うしろ　レフリーかどうか False=レフリーではない

YOLO format: path/to/img1.jpg 50(x1),100(y1),150(x2),200(y2),0(class) 30,50,200,120,3...

class:
    0 person
    1 No-A 0
'''

import os
import glob
from tqdm import tqdm
import time

def id_converter(class_id):
    """
    15->14
    16->15
    17->16
    19->17
    20->18
    30->19
    40->20
    """
    if class_id == 16:
        class_id = 15
    elif class_id == 17:
        class_id = 17
    elif class_id == 19:
        class_id = 17
    elif class_id == 20:
        class_id = 18
    elif class_id == 30:
        class_id = 19
    elif class_id == 40:
        class_id = 20
    return str(class_id)


def main():
    output_file = "yolo_texts/yolo_format_all_resize_0813.txt"
    parent = glob.glob("data/all/*")
    a = []
    b = []
    c = []
    d = []
    e = []
    g = []
    invalid = 0
    from collections import defaultdict
    bbox_info = defaultdict(list)

    for child in parent:
        files = glob.glob(child + "/*.csv")
        for filename in files:
            f = open(filename)
            # img_name = filename.replace(".csv",".jpg")
            # img = cv2.imread(img_name)
            # hh,ww,_ = img.shape
            # yoko_ratio = ww/4
            info_per_img = []
            for line in f.readlines():
                info = line.strip().split(",")
                x1 = int(info[1].strip('"').replace(".0",""))
                y1 = int(info[2].strip('"').replace(".0",""))
                w = int(info[3].strip('"').replace(".0",""))
                h =  int(info[4].strip('"').replace(".0",""))
                class_name = info[5]
                referee = info[len(info)-1]
                if "No-" in class_name:
                    player_id =info[7]
                    player_id = player_id.strip('"')
                    if player_id != "":
                        if 'P' not in player_id and 'p' not in player_id and 'o' not in player_id:
                            player_id = int(player_id)
                            if "A" in class_name:
                                a.append(player_id)
                            elif "B" in class_name:
                                b.append(player_id)
                            elif "C" in class_name:
                                c.append(player_id)
                            elif "D" in class_name:
                                d.append(player_id)
                            elif "E" in class_name:
                                e.append(player_id)
                            elif "F" in class_name:
                                g.append(player_id)
                        else:
                            invalid+=1
                            player_id = 14 #None
                    else:
                        invalid+=1
                        player_id = 14 #None
                elif "Ball" in class_name:
                    player_id = 13 #Ball
                elif referee == '"True"':
                    player_id = 10 #Referee
                elif class_name == '"Head"':
                    player_id = 11 #Head
                elif class_name == '"Person"':
                    player_id = 12 #Person
                # player_id = id_converter(player_id)
                info_per_img.append((x1,y1,x1+w,y1+h,int(player_id)))
            filename = filename.strip("/data/all/")
            bbox_info[filename].append(info_per_img)

    with open(output_file, mode='w') as f:
        f.writelines("")

    for path in bbox_info.keys():
        for bboxes in tqdm(bbox_info[path]):
            i = 0
            for i, bbox in enumerate(bboxes):
                x1 = int(bbox[0]/4)
                y1 = int(bbox[1]/4)
                x2 = int(bbox[2]/4)
                y2 = int(bbox[3]/4)
                class_id = bbox[4]
                if i == 0:
                    with open(output_file, mode='a') as f:
                        f.write("resize_all" + str() + path.replace(".csv",".jpg")+" ")
                with open(output_file, mode='a') as f:
                    f.write(str(x1) + "," + str(y1) + "," + str(x2) + "," + str(y2) + "," + str(class_id) + " ")
                if i == len(bboxes)-1:
                    with open(output_file, mode='a') as f:
                        f.write("\n")

    import collections
    A = collections.Counter(a)
    B = collections.Counter(b)
    C = collections.Counter(c)
    D = collections.Counter(d)
    E = collections.Counter(e)
    F = collections.Counter(g)
    print("a:",A)
    print("b:",B)
    print("c:",C)
    print("d:",D)
    print("e:",E)
    print("f:",F)
    print("invalid:",invalid)
if __name__ == "__main__":
    main()
