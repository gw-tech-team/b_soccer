
def main():
    dir_name = "format_files"
    project_name = "bsoccer_a_all"
    # class_list = ["No.0","No.1","No.2","No.3","No.4","No.5","No.6","No.7","No.8","No.9",
    #                 "Referee","Head","Person","Ball","None"]
    class_list = ["No.0","No.1","No.2","No.3","No.4","No.5","No.6","No.7","No.8","No.9","Referee","Head","Person","Ball"]
    size = (416,416) #(width,height)

    len_class = len(class_list)

    """create .names file"""
    with open(dir_name + "/" + project_name + ".names", mode = "w") as f:
        for name in class_list:
            f.write(name+"\n")

    """create .cfg file"""
    with open(dir_name + "/yolov3-" + project_name + ".cfg", mode = "w") as g:
        with open(dir_name + "/" + "format.cfg", mode="r") as h:
            lines = h.readlines()
            for i, line in enumerate(lines):
                if i == 7:
                    line = line.split("=")
                    line[1] = str(size[0]) + "\n"
                    line = "=".join(line)
                elif i == 8:
                    line = line.split("=")
                    line[1] = str(size[1]) + "\n"
                    line = "=".join(line)
                elif i == 19:
                    line = line.split("=")
                    max_batches = len_class*2000
                    line[1] = str(max_batches) + "\n"
                    line = "=".join(line)
                elif i == 21:
                    line = line.split("=")
                    steps_1 = int(max_batches*0.8)
                    steps_2 = int(max_batches*0.9)
                    line[1] = str(steps_1) + "," + str(steps_2) + "\n"
                    line = "=".join(line)
                elif i == 609 or i == 695 or i == 782:
                    line = line.split("=")
                    line[1] = str(len_class) + "\n"
                    line = "=".join(line)
                elif i == 602 or i == 688 or i == 775:
                    line = line.split("=")
                    filters = (len_class+5)*3
                    line[1] = str(filters) + "\n"
                    line = "=".join(line)
                g.write(line)

    """create .data file"""
    with open(dir_name + "/" + project_name + ".data", mode = "w") as l:
        with open(dir_name + "/" + "format.data", mode="r") as m:
            lines = m.readlines()
            for i, line in enumerate(lines):
                if i == 0:
                    line = line.split("=")
                    line[1] = str(len_class) + "\n"
                    line = "=".join(line)
                elif i == 4:
                    line = line.split("=")
                    line[1] = "./" + project_name + ".names" + "\n"
                    line = "=".join(line)
                l.write(line)


if __name__ == "__main__":
    main()
